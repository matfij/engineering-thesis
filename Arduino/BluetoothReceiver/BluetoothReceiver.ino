#include <SoftwareSerial.h>

// bluetooth connection
SoftwareSerial bluetoothSocket(62, 63);
char message;

int randNumber;
int input;

void setup()
{
  // start serial communication at 115200bps 
  Serial.begin(9600);
  bluetoothSocket.begin(9600);
  Serial.println("Program started.");

  
}

 
void loop() 
{

  delay(150);
  randNumber = random(9);
  bluetoothSocket.print(randNumber);

  if( bluetoothSocket.available() ) {
    input = bluetoothSocket.read();
  Serial.println(input);
    
  }
  
}
