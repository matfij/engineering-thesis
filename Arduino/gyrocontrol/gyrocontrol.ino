#include <MPU6050_tockn.h>
#include <Wire.h>

MPU6050 mpu6050(Wire);
double angX = 0, angY = 0;
double errX = 0, errY = 0;
double buffX, buffY = 0;
double X, Y;

void setup() 
{
  Serial.begin(9600);
  Serial3.begin(9600);
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
}


void loop() 
{
  configurate();

  getAngles();

  printErrors();
} 


void getAngles()
{
  double tempX = 0, tempY = 0;
  
  for (int i = 1; i <= 10; i++)
  {
    mpu6050.update();
    tempX += 0.5 * (buffX + mpu6050.getAngleX());
    tempY += 0.5 * (buffY + mpu6050.getAngleY());
  }
  buffX = tempX/10;
  buffY = tempY/10;
  angX = buffX - errX;
  angY = buffY - errY;

}

void printErrors()
{ 
  Serial.print("X : ");Serial.print(angX);
  Serial.print(" - Y : ");Serial.println(angY);
}


void configurate()
{
  if (Serial3.available())
  {
    char t = Serial3.read();
    Serial.println(t);
    if (t != 'x')
    {
      errX = angX;
      errY = angY; 
    }
  }
}
