#include <MPU6050_tockn.h>
#include <Wire.h>


// state control
bool start = false;

// gyroscope control
MPU6050 mpu6050(Wire);
double angX;  // roll
double angY;  // pitch

// engine control
#define northMotA 11
#define northMotB 10
#define eastMotA 9
#define eastMotB 8
// x axis
#define weastMotA 4
#define weastMotB 5
#define southMotA 6
#define southMotB 7

// pid control
float gain = 1, dt = 10;
float Kp = 35, Ki = 0, Kd = 0;
float errorX = 0, lastErrorX = 0, errorY = 0, lastErrorY = 0;
float iErrorX = 0, dErrorX = 0, iErrorY = 0, dErrorY = 0;
float acceptedError = 0.09;
float baseErrorX = 0, baseErrorY = 0;

// ab estimator
float A = 0.45, B = 0.05;
float X = 0, nextX = 0, Y = 0, nextY = 0;
float VX = 0, nextVX = 0, VY = 0, nextVY = 0;


void setup() 
{
  Stop();
  
  // initialize serial communication;
  Serial.begin(9600);
  Serial3.begin(9600);
  Wire.begin();
  Wire.setClock(400000L);

  // initialize gyro mpu
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);

  // activate engine pins
  pinMode(northMotA, OUTPUT);
  pinMode(northMotB, OUTPUT);
  pinMode(weastMotA, OUTPUT);
  pinMode(weastMotB, OUTPUT);
  pinMode(eastMotA, OUTPUT);
  pinMode(eastMotB, OUTPUT);
  pinMode(southMotA, OUTPUT);
  pinMode(southMotB, OUTPUT);

  // set base error
  baseErrorX = mpu6050.getAngleY();
  baseErrorY = mpu6050.getAngleX();
}

void loop()
{
  // read gyro input
  ReadInput();

  ReadControls();

  if (start)
  {
    // calculate controller outputs
    CalculateErrors();
    
    // empower engines
    CalculateOutpus(); 
  }
  else
  {
    Stop();
  }
} 


void ReadInput()
{
  int callen = 50;
  double tempX[callen], tempY;

  for (int i = 0; i < callen; i++)
  {
    mpu6050.update();
    
    tempX[i] = 0.5 * (angX + mpu6050.getAngleX());
    tempY += 0.5 * (angY + mpu6050.getAngleY());
  }
  for (int i = 0; i < callen-1; i++)      
  {
    for (int j = 0; j < callen-i-1; j++)  
    {
      if (tempX[j] > tempX[j+1])
      {
        float buff = tempX[j];
        tempX[j] = tempX[j+1];
        tempX[j+1] = buff;
      }
    }
  }
  angX = tempX[23];
  angY = tempY/callen;
  X = angX - baseErrorX;
  Y = angY - baseErrorY;

  Serial.print(X);Serial.print(" ");Serial.println(Y);
}




void ReadControls()
{
  if (Serial3.available()) 
  {
    char in = Serial3.read();
    if (in == 's')
    {
      baseErrorX = angX;
      baseErrorY = angY;
    }
    if (in == 'r')
    {
      start = true;
    }
    if (in == 'c')
    {
      start = false;
    }
  }
}


void CalculateErrors()
{
  // proportional part
  if (X > 0) errorX = X*X;
  else errorX = -X*X;
  if (Y > 0) errorY = Y*Y;
  else errorY = -Y*Y;

  // integral part
  iErrorX = iErrorX + dt * errorX;
  iErrorY = iErrorY + dt * errorY;

  // derivative part
  dErrorX = (errorX - lastErrorX) / dt;
  dErrorY = (errorY - lastErrorY) / dt;

  lastErrorX = errorX;  
  lastErrorY = errorY;  
}


void CalculateOutpus()
{
  float outputX = gain * (Kp * errorX + Ki * iErrorX + Kd * dErrorX);
  float outputY = gain * (Kp * errorY + Ki * iErrorY + Kd * dErrorY);

  if (errorX > acceptedError)
  {
    int ride = min(abs(outputX), 255); 
    rideX(0, ride, 0, ride);
  }
  else if (-errorX > acceptedError)
  {
    int ride = min(abs(outputX), 255); 
    rideX(ride, 0, ride, 0);
  }
  else
  {
    rideX(0, 0, 0, 0);
    iErrorX = 0;
  }

  if (errorY > acceptedError)
  {
    int ride = min(abs(outputY), 255); 
    rideY(ride, 0, ride, 0);
  }
  else if(-errorY > acceptedError)
  {
    int ride = min(abs(outputY), 255);   
    rideY(0, ride, 0, ride);
  }
  else
  {
    rideY(0, 0, 0, 0);
    iErrorY = 0;
  }
  
}


void rideX(int wa, int wb, int sa, int sb)
{
  analogWrite(weastMotA, wa);
  analogWrite(weastMotB, wb);

  analogWrite(southMotA, sa);
  analogWrite(southMotB, sb);
}

void rideY(int na, int nb, int ea, int eb)
{
  analogWrite(northMotA, na);
  analogWrite(northMotB, nb);

  analogWrite(eastMotA, ea);
  analogWrite(eastMotB, eb);
}

void Stop()
{
  analogWrite(weastMotA, 0);
  analogWrite(weastMotB, 0);
  analogWrite(southMotA, 0);
  analogWrite(southMotB, 0);

  analogWrite(northMotA, 0);
  analogWrite(northMotB, 0);
  analogWrite(eastMotA, 0);
  analogWrite(eastMotB, 0);
}
