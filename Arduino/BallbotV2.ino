#include <MPU6050_tockn.h>
#include <Wire.h>
#include <PID_v1.h>

// state control
bool start = false;

// gyroscope control
MPU6050 mpu6050(Wire);
double X = 0, Y = 0;
double lastX = 0, lastY = 0;

// engine control
#define northMotA 11
#define northMotB 10
#define eastMotA 9
#define eastMotB 8
// x axis
#define weastMotA 4
#define weastMotB 5
#define southMotA 6
#define southMotB 7

// timers
long previousMillis = 0;
long currentMillis;    
long interval = 10; 
double dt = interval/1000;

// pid control
double gain = 1.4, minGain = 31;
double Kp = gain*15, Ki = gain*0.7, Kd = gain*0.1;
double outputX, outputY, setPoint = 0;
float baseErrorX = 0, baseErrorY = 0, acceptedError = 0.1;
float angX = 0, angY = 0;

PID pidX(&X, &outputX, &setPoint, Kp, Ki, Kd, DIRECT);
PID pidY(&Y, &outputY, &setPoint, Kp, Ki, Kd, DIRECT);


void setup() 
{
  Stop();
  
  // initialize serial communication;
  Serial.begin(9600);
  Serial3.begin(9600);
  Wire.begin();
  Wire.setClock(400000L);

  // initialize gyro mpu
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);

  // activate engine pins
  pinMode(northMotA, OUTPUT);
  pinMode(northMotB, OUTPUT);
  pinMode(weastMotA, OUTPUT);
  pinMode(weastMotB, OUTPUT);
  pinMode(eastMotA, OUTPUT);
  pinMode(eastMotB, OUTPUT);
  pinMode(southMotA, OUTPUT);
  pinMode(southMotB, OUTPUT);

  // set PIDs
  pidX.SetMode(AUTOMATIC);
  pidX.SetOutputLimits(-255, 255);
  pidY.SetMode(AUTOMATIC);
  pidY.SetOutputLimits(-255, 255);
  pidX.SetSampleTime(interval);
  pidY.SetSampleTime(interval);
}

void loop()
{
  ReadControls();
  ControlMotors(); 

  if (start)
  {
    EmpowerMotors();
  }
  else
  {
    Stop();
  }
} 


void ReadInput()
{
  double tempX = 0, tempY = 0;

  for (int i = 0; i < 50; i++)
  {
    mpu6050.update();
    
    tempX += 0.5 * (angX + mpu6050.getAngleX());
    tempY += 0.5 * (angY + mpu6050.getAngleY());
  }
  
  angX = tempX/50;
  angY = tempY/50;
  X = angX - baseErrorX;
  Y = angY - baseErrorY;

  Serial.print(X);Serial.print(" ");Serial.println(Y);
}


void ReadControls()
{
  if (Serial3.available()) 
  {
    char in = Serial3.read();
    if (in == 's')
    {
      baseErrorX = angX;
      baseErrorY = angY;
    }
    if (in == 'r')
    {
      start = true;
    }
    if (in == 'c')
    {
      start = false;
    }
  }
}


void ControlMotors()
{
  currentMillis = millis();
  
  if (currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;

    ReadInput();
 
    pidY.Compute();
    outputY = abs(outputY);
    outputY = outputY*gain;
    outputY = map(outputY, 0, 255, minGain, 255);
    outputY = constrain(outputY, 0, 255);
    
    pidX.Compute();
    outputX = abs(outputX);  
    outputX = outputX*gain;
    outputX = map(outputX, 0, 255, minGain, 255);
    outputX = constrain(outputX, 0, 255);
  }
}


void EmpowerMotors()
{
  if (X > acceptedError)
  {
    int ride = min(abs(outputX), 255); 
    rideX(0, ride, 0, ride);
  }
  else if (-X > acceptedError)
  {
    int ride = min(abs(outputX), 255); 
    int rideCompensation = min(ride + 30, 255);
    rideX(ride, 0, ride, 0);
  }
  else
  {
    rideX(0, 0, 0, 0);
  }

  if (Y > -0.75*acceptedError)
  {
    int ride = min(abs(outputY), 255); 
    rideY(ride, 0, ride, 0);
  }
  else if(-Y > 1.20*acceptedError)
  {
    int ride = min(abs(outputY), 255);   
    rideY(0, ride, 0, ride);
  }
  else
  {
    rideY(0, 0, 0, 0);
  }
  
}


void rideX(int wa, int wb, int sa, int sb)
{
  analogWrite(weastMotA, wa);
  analogWrite(weastMotB, wb);

  analogWrite(southMotA, sa);
  analogWrite(southMotB, sb); 
}

void rideY(int na, int nb, int ea, int eb)
{
  analogWrite(northMotA, na);
  analogWrite(northMotB, nb);

  analogWrite(eastMotA, ea);
  analogWrite(eastMotB, eb); 
}

void Stop()
{
  analogWrite(weastMotA, 0);
  analogWrite(weastMotB, 0);
  analogWrite(southMotA, 0);
  analogWrite(southMotB, 0);

  analogWrite(northMotA, 0);
  analogWrite(northMotB, 0);
  analogWrite(eastMotA, 0);
  analogWrite(eastMotB, 0);
}
