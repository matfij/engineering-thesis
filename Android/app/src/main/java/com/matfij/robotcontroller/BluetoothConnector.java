package com.matfij.robotcontroller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import java.util.Set;
import java.util.UUID;

public final class BluetoothConnector
{
    // current view context
    Context context;

    // data fot bluetooth connection
    private static BluetoothAdapter bluetoothAdapter = null;
    private static BluetoothSocket bluetoothSocket = null;
    private static String adapterAddress = null;
    private static String adapterName = null;
    private static Set<BluetoothDevice> pairedDevice;
    private static final UUID controlAdapterUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    public static boolean ConnectDevice()
    {
        try
        {
            // access phone bluetooth socket
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            adapterAddress = bluetoothAdapter.getAddress();

            // access paired device
            pairedDevice = bluetoothAdapter.getBondedDevices();

            if(pairedDevice.size() > 0)
            {
                for(BluetoothDevice bluetoothDevice: pairedDevice)
                {
                    adapterAddress = bluetoothDevice.getAddress();
                    adapterName = bluetoothDevice.getName();
                }
            }

            // initialize bluetooth connection objects
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(adapterAddress);
            bluetoothSocket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(controlAdapterUUID);

            // finalize connection
            bluetoothSocket.connect();

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public static void SendMessage(char message)
    {
        try
        {
            bluetoothSocket.getOutputStream().write(message);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
