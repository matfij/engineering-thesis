package com.matfij.robotcontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public final class MenuViewModel extends AppCompatActivity
{
    // control buttons
    private Button connectBtn;
    private Button[] buttons = new Button[6];

    private final char[] controlMessages = {'r', 's', 'c'};
    private final char exitMessage = 'x';

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // connect view buttons
        connectBtn = findViewById(R.id.ConnectBtn);

        buttons[0] = (Button)findViewById(R.id.startBtn);
        buttons[1] = (Button)findViewById(R.id.calibrateBtn);
        buttons[2] = (Button)findViewById(R.id.stopBtn);

        AddButtonListeners();
    }

    private void AddButtonListeners()
    {
        // listener for a connection button
        connectBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(!BluetoothConnector.ConnectDevice())
                {
                    Toast.makeText(getApplicationContext(), "Connection failed!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Connected.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // listeners for the movement control buttons
        for(int cnt = 0; cnt < 3; cnt++)
        {
            Log.e("mntag", ""+cnt);
            final int i = cnt;

            // add listener to specific button
            buttons[cnt].setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View view, MotionEvent event)
                {
                    // send specific char on click
                    if(event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        BluetoothConnector.SendMessage(controlMessages[i]);
                    }
                    // send exit char on release
                    else if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        BluetoothConnector.SendMessage(exitMessage);
                    }
                    return false;
                }
            });
        }
    }
}
