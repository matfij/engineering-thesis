Solver 1691.450 Linear-static-solver-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1691.450 Loading-and-preparing-input-data-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1691.872 Loading-and-preparing-input-data-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1691.872 Computing-load-vector-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1691.952 Computing-load-vector-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1691.995 FFEPlus-Solver-start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1694.258 FFEPlus-Solver-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1694.764 Computing-stresses-for-load-case=1-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1694.764 Computing-reaction-forces-for-load-case(LCASE)=1-Start 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1695.718 Computing-reaction-forces-for-load-case(LCASE)=1-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1695.741 Computing-stresses-for-load-case=1-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
Solver 1695.779 Linear-static-solver-End 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
SOLVER TYPE= FFEPlus (PCG Iterative Solver)
Number of Equations (NEQ)=277305
Number of Nodal points (NUMNP)=92466
Number of Pseudo nodes (NUMPS)=0
Number of Elements (NUME)=58113
Number of Pseudo Elements (NUMP)=0
SOLUTION TIME LOG
Time for Input Data Transfer from Database =0.619000
TIME FOR CALCULATION OF STRUCTURE STIFFNESS MATRIX (STF_PHASE)=0.008000
TIME FOR FFEPlus (PCG ITERATIVE SOLVER) =2.302000
TIME FOR GAP & CONTACT ITERATIONS (CNT_PHASE)=0.000000
TIME FOR REACTION FORCE & DISP UPDATE TO POST (REC_PAHS)=0.502000
TIME FOR STRESS CALCULATION (STS_PHASE)=0.977000
TIME FOR WRITING POST PROCESSING INFORMATION (PST_PHASE)=0.006000
TOTAL SOLUTION TIME (TOT_TIME)=4.407000
