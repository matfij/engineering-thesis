function varargout = GyroPresenter(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GyroPresenter_OpeningFcn, ...
                   'gui_OutputFcn',  @GyroPresenter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function GyroPresenter_OpeningFcn(hObject, eventdata, handles, varargin)

    handles.output = hObject;

    guidata(hObject, handles);
    
    % setup
    clc
%     clear Arduino
%     delete(instrfind({'Port'}, {'COM3'}))
%     
%     global Arduino;
%     Arduino = arduino('COM3')
%     

function varargout = GyroPresenter_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;


% --- Executes on button press in LoadBtn.
function LoadBtn_Callback(hObject, eventdata, handles)

    % open port
    port = serial('COM3');
    set(port, 'BaudRate', 9600)
    fopen(port);
    
    for ind = 1 : 1 : handles.scale
       
        % read serial data
        if (mod(ind, 2) == 0)
            x = fscanf(port, '%e', 12)
        else
            y = fscanf(port, '%e', 12)
        end
        
        % display serial data
        if (ind > 40)
            
            try
                
                hold on
                plot(ind, x, '.b', ind, y, '.r');  
                
                ylim([-10 10])
                xlabel('frames')
                ylabel('angle[deg]')
                legend('x - roll', 'y - pitch')
            catch
            end
        end
    end
    
    % close port
    fclose(port);
    delete(port);
    clear sss
    

function SettingsField_Callback(hObject, eventdata, handles)

    handles.data = get(hObject, 'String')
    handles.scale = str2double(handles.data)
    guidata(hObject, handles)

function SettingsField_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
