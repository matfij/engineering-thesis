function PID(handles, G, Kp, Kd, Ki, t)

% controller
Gpid = pid(Kp, Kd, Ki);
G = feedback(G, Gpid);

% simulation
if get(handles.stepCheck, 'value')
    hold on
    [y, ti] = step(G, t);
    plot(ti, y, 'b')
    title('Step response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('PID', 'FontSize', 15)
end
if get(handles.impulseCheck, 'value')
    hold on
    [y, ti] = impulse(G, t);
    plot(ti, y, 'b')
    title('Impulse response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('PID', 'FontSize', 15)
end

grid on
zoom on

% gui update
aText = sprintf(' Kp = %1.2f \n Ki = %1.2f \n Kd = %1.2f', Kp, Ki, Kd);
set(handles.regulatorProperties, 'String', aText);