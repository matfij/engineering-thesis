function LQR(handles, A, B, C, D, cost, t)

% control weights
w1 = 1 + 100*cost;
w2 = 1 + 1000*cost;
w3 = 1 + 100*cost;
w4 = 1 + 100*cost;

Q = [w1 0 0 0;
     0 w2 0 0;   
     0 0 w3 0;
     0 0 0 w4];
 
% lqr tuning
K = lqr(A, B, Q, 1);
Gz = ss(A-B*K, B, C, D);

% simulation

if get(handles.stepCheck, 'value')
    hold on
    [y, ti] = step(Gz, t);
    plot(ti, y, 'm')
    title('Step response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('LQR', 'FontSize', 15)
end
if get(handles.impulseCheck, 'value')
    hold on
    [y, ti] = impulse(Gz, t);
    plot(ti, y, 'm')
    title('Impulse response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('LQR', 'FontSize', 15)
end

grid on
zoom on

% gui update
aText = sprintf('K = [%1.1f  %1.1f  %1.1f  %1.1f]', K(1), K(2), K(3), K(4));
set(handles.regulatorProperties, 'String', aText);