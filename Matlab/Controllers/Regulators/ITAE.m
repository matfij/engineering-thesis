function ITAE(handles, G, rs, bKp, bKi, bKd, cf, dr, t)

% optimization options
global Go cost response simTime regStruct
cost = cf;
response = dr;
Go = G;
simTime = t;
regStruct = rs;
options = optimset(@fminsearch);

Gpid = pid(bKp, bKi, bKd);

% initial conditions based on regulator structure
switch rs
    case 1 % PI
        K = [bKp, bKi];
        oK = abs(fminsearch(@fobs, K, options));
        Gpid = pid(oK(1), oK(2), 0);
        
        aText = sprintf(' Kp = %1.2f \n Ki = %1.2f \n Kd = %1.2f', oK(1), oK(2), 0);
        set(handles.regulatorProperties, 'String', aText);

    case 2 % PD
        K = [bKp, bKd];
        oK = abs(fminsearch(@fobs, K, options));
        Gpid = pid(oK(1), 0, oK(2));
        
        aText = sprintf(' Kp = %1.2f \n Ki = %1.2f \n Kd = %1.2f', oK(1), 0, oK(2));
        set(handles.regulatorProperties, 'String', aText);

    case 3 % PID
        K = [bKp, bKi, bKd];
        oK = abs(fminsearch(@fobs, K, options));
        Gpid = pid(oK(1), oK(2), oK(3));
        
        aText = sprintf(' Kp = %1.2f \n Ki = %1.2f \n Kd = %1.2f', oK(1), oK(2), oK(3));
        set(handles.regulatorProperties, 'String', aText);
end

% verification
Gz = feedback(Go, Gpid);

if get(handles.stepCheck, 'value')
    hold on
    [y, ti] = step(Gz, t);
    plot(ti, y, 'b')
    title('Step response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('ITAE', 'FontSize', 15)
end
if get(handles.impulseCheck, 'value')
    hold on
    [y, ti] = impulse(Gz, t);
    plot(ti, y, 'b')
    title('Impulse response', 'FontSize', 15)
    xlabel('time [s]', 'FontSize', 15)
    ylabel('amplitude [rad]', 'FontSize', 15)
    legend('ITAE', 'FontSize', 15)
end

grid on
zoom on