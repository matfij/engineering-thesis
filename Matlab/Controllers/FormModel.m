function [G, A, B, C, D] = FormModel(handles, m1, m2, r1, r2, h)

% system geometry constants
g = 9.81;
I1 = 0.4*m1*r1^2;
I2 = (m1*h^2)/3 + m1*r1^2;

% dynamic model
M = [ ( r1^2*(m1*+m2)+I1 ) ( m2*h*r1 );
      ( m2*r1*h ) ( m2*h^2+I2 )];
  
C = [0];

K = [-m1*g*h 0; 
     0 0];

% state model
a1 = (m2*g*h*M(1,2)) / det(M);
a2 = (m2*g*h*M(1,1)) / det(M);
b1 = (M(2,2)+M(1,2)) / det(M);
b2 = (M(2,1)+M(1,1)) / det(M);

A = [0 0 1 0;
     0 0 0 1;
     0 a1 0 0;
     0 a2 0 0];
 
B = [0; 0; b1; b2];

C = [0 1 0 0];

D = 0;

% transfer function model
[b, a] = ss2tf(A, B, C, D);
G = tf(b, a);

% gui update
aText = sprintf([repmat(' %1.0f ', 1, size(A,2)) '\n'], A');
set(handles.stateMatrix, 'String', aText);

bText = sprintf([repmat(' %1.0f ', 1, size(B,2)) '\n'], B');
set(handles.inputMatrix, 'String', bText);






