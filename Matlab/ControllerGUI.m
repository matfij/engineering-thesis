function varargout = ControllerGUI(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ControllerGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ControllerGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
addpath Controllers
addpath Controllers/Regulators
addpath Utilities
function ControllerGUI_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = ControllerGUI_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in simulateButton.
function simulateButton_Callback(hObject, eventdata, handles)
    try
        m1 = str2double(get(handles.m1Input, 'String'));
        m2 = str2double(get(handles.m2Input, 'String'));
        r1 = str2double(get(handles.r1Input, 'String'));
        r2 = str2double(get(handles.r2Input, 'String'));
        h = str2double(get(handles.hInput, 'String'));

        t = str2double(get(handles.timeInput, 'String'));
        method = get(handles.regMenu, 'Value');
        
        Kp = str2double(get(handles.KpInput, 'String'));
        Kd = str2double(get(handles.KdInput, 'String'));
        Ki = str2double(get(handles.KiInput, 'String'));
        cost = get(handles.lqrSlider, 'Value');
        rs = get(handles.pidMenu, 'Value');
        bKp = str2double(get(handles.BKpInput, 'String'));
        bKi = str2double(get(handles.BKiInput, 'String'));
        bKd = str2double(get(handles.BKdInput, 'String'));
        cf = str2double(get(handles.cfInput, 'String'));
        dr = get(handles.drInput, 'Value');
    catch
        msgbox('Incorrect Input', 'Warning','warn');
    end
%     try
        [G, A, B, C, D] = FormModel(handles, m1, m2, r1, r2, h);
        
        switch method
            case 1
                PID(handles, G, Kp, Kd, Ki, t)
            case 2
                ITAE(handles, G, rs, bKp, bKi, bKd, cf, dr, t)
            case 3
                LQR(handles, A, B, C, D, cost, t)
        end
%     catch
%         msgbox('Simulation Error', 'Error','error');
%     end
        

function clearButton_Callback(hObject, eventdata, handles)
    axes(handles.mainAxes)
    cla reset
    set(handles.inputMatrix, 'String', '');
    set(handles.stateMatrix, 'String', '');
    set(handles.regulatorProperties, 'String', '');
    clc
    
    
    
    
    
    
    
    
