% clc
clear
close all

% system geometry constants
m1 = 1; m2 = 2.5;
r1 = 0.265; r2 = 0.0324;
h = 0.3; g = 9.81;
I1 = 0.4*m1*r1^2; I2 = (m1*h^2)/3 + m1*r1^2;

% dynamic model
M = [ ( r1^2*(m1*+m2)+I1 ) ( m2*h*r1 );
      ( m2*r1*h ) ( m2*h^2+I2 )];

K = [-m1*g*h 0; 
     0 0];
 
a1 = (m2*g*h*M(1,2)) / det(M);
a2 = (m2*g*h*M(1,1)) / det(M);
b1 = (M(2,2)+M(1,2)) / det(M);
b2 = (M(2,1)+M(1,1)) / det(M);

% state model
A = [0 0 1 0;
     0 0 0 1;   
     0 a1 0 0;
     0 a2 0 0];
 
B = [0; 0; b1; b2];

C = [0 1 0 0];

D = 0;

% transfer function model
dt = 0.001;
[b, a] = ss2tf(A, B, C, D);
G = tf(b, a);

pid = pid(22, 99, 1.3);
Gpid = tf(pid);

Go = series(G, Gpid);
Gsys = feedback(Go, 1);

% response simulation
[u, t] = gensig('square', 1, 10, 0.01);
subplot(1, 2, 1)
lsim(Gsys, u, t)
grid on
subplot(1, 2, 2)
impulse(Gsys, t)
grid on

