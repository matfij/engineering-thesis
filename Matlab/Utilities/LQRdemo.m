clc
clear
close all

% system model
A = [0 0 1 0;
     0 0 0 1;   
     0 54.7 0 0;
     0 56.1 0 0];
 
B = [0; 0; 19.6; 15.1];

C = [0 1 0 0];

D = 0;

% control weights
Q = [10 0 0 0;
     0 25 0 0;   
     0 0 5 0;
     0 0 0 5];

% lqr calculation
K = lqr(A, B, Q, 1);
sys = ss(A-B*K, B, C, D);

% system simulation 
impulse(sys)


