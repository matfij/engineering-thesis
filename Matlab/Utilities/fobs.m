function f = fobs(k)

% simulation parameters
global cost response simTime Go regStruct
dt = 0.01;
t = 0 : dt : simTime;

% closed loop system
switch regStruct
    
    case 1 % PI
        Gpid = pid(k(1), k(2), 0);
        Gsys = feedback(Go, Gpid);
        
        switch response
            case 1
                y = step(Gsys, t);
            case 2
                y = impulse(Gsys, t);
            case 3
                for n = 1 : 1 : length(t)
                    u(n) = 0;
                    if (n >= 10 && n <= 20)
                        u(n) = 2;
                    end
                end
                y = lsim(Gsys, u, t);
        end

        f = 0;
        for n = 1 : 1 : length(t)
            if n > 10
                f = f + t(n) * abs(y(n)).^2 + cost * (abs(k(1)) + abs(k(2)) + 0);
            end
        end
        
    case 2 % PD
        Gpid = pid(k(1), 0, k(2));
        Gsys = feedback(Go, Gpid);
        
        switch response
            case 1
                y = step(Gsys, t);
            case 2
                y = impulse(Gsys, t);
            case 3
                for n = 1 : 1 : length(t)
                    u(n) = 0;
                    if (n >= 10 && n <= 20)
                        u(n) = 2;
                    end
                end
            y = lsim(Gsys, u, t);
        end
        
        f = 0;
        for n = 1 : 1 : length(t)
            if n > 10
                f = f + t(n) * abs(y(n)).^2 + cost * (abs(k(1)) + abs(k(2)) + 0);
            end
        end
        
    case 3 % PI
        Gpid = pid(k(1), k(2), k(3));
        Gsys = feedback(Go, Gpid);

        switch response
            case 1
                y = step(Gsys, t);
            case 2
                y = impulse(Gsys, t);
            case 3
                for n = 1 : 1 : length(t)
                    u(n) = 0;
                    if (n >= 10 && n <= 20)
                        u(n) = 2;
                    end
                end
                y = lsim(Gsys, u, t);
        end
        
        f = 0;
        for n = 1 : 1 : length(t)
            if n > 10
                f = f + t(n) * abs(y(n)).^2 + cost * (abs(k(1)) + abs(k(2)) + 0);
            end
        end
end
