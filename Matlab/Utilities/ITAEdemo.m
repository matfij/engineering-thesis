clc
clear
close

global Go
global cost response
cost = 0.00;
response = 1;

% plant
Go = tf([15 0 0], [1 0 -56 0 0]);

% itae optimization
kp = 3.5693;
ki = 0.1151;
kd = 0;
K = [kd ki];

options = optimset('TolX',2);
oK = fminsearch(@fobs, K, options);

% verification
oK%
Gpid = pid(15, 25, 2);
% Gpid = pid(oK(1), oK(2), 0);

Gsys = feedback(Go, Gpid);

dt = 0.01;
t = 0 : dt : 10;
subplot(1, 2, 1)
impulse(Gsys, t)
grid on
subplot(1, 2, 2)
step(Gsys, t)
grid on

