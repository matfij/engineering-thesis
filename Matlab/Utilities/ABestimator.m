clc
clear

% Parametry symulacji
dt = 0.01;
t_end = 10;
t = 0:dt:t_end;
 
% Parametry filtru
alfa = 0.11;
beta = 0.05;
 
Y(1) = 0;
Yf = Y;
 
ypri = 0;
ypost = 0;
vpri = 0;
vpost = 0;


for i = 1 : size(t,2)
    
    Y(i) = 2*sin(0.006*i) + 0.27*rand(1) - 0.27*rand(1);
    Z(i) =  2*sin(0.006*i) + 0.04*rand(1) - 0.06*rand(1);
    ypri = ypost + dt*vpost;
    vpri = vpost;
    ypost = ypri + alfa*(Y(i) - ypri);
    vpost = vpri + beta*(Y(i) - ypri)/dt;
    Yf(i) = ypost;
end
 
plot(t, Y, '-m')
hold on
plot(t, Z, '-k', 'LineWidth', 2)
title('Alfa-Beta Filtration')
xlabel('time [s]')
ylabel('amplitude [deg]')
legend('Raw value', 'Estimated value')


